import java.util.ArrayList;
import java.util.Collections;
import javax.swing.JTextField;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Polynomial {

	private ArrayList<Monomial> polinom1 = new ArrayList<>();
	private ArrayList<Monomial> polinom2 = new ArrayList<>();

	Polynomial() {
		super();
	}

	/*Polynomial(ArrayList<Monom> polinom1, ArrayList<Monom> polinom2) {
		this.polinom1 = polinom1;
		this.polinom2 = polinom2;
	}*/

	public ArrayList<Monomial> Split_Regex(String format) { //polinom <-string
		ArrayList<Monomial> polinom = new ArrayList<>();
		Pattern pattern = Pattern.compile("(-?\\b\\d+)[x]\\^(-?\\d+\\b)");
		Matcher matcher = pattern.matcher(format);
		while (matcher.find()) {
			double coef = Double.parseDouble(String.valueOf(matcher.group(1)));
			int grad = Integer.parseInt(String.valueOf(matcher.group(2)));
			polinom.add(new Monomial(coef, grad));
		}
		return polinom;
	}

    ArrayList<Monomial> Addition(String f1, String f2, JTextField out) { //adunare
		ArrayList<Monomial> sum = new ArrayList<>();		   //polinom rezultat (suma)
		polinom1 = Split_Regex(f1);                          //polinom1<-string1
		polinom2 = Split_Regex(f2);                          //polinom2<-string2
		Collections.sort(polinom1, Monomial.Comparators.GRAD);
		Collections.sort(polinom2, Monomial.Comparators.GRAD);
		
		Monomial s = new Monomial(0.0, 0);			// monom ce se adauga la suma
						
		for (Monomial m1 : polinom1) {				
			for (Monomial m2 : polinom2) {
				if (m1.getPower() == m2.getPower()) {
												// insumarea coeficientiilor de aceeasi putere
					s.setCoef(m1.getCoef() + m2.getCoef());
					s.setPower(m1.getPower());
					m1.setPower(-1);
					m2.setPower(-1);
					sum.add(s);
				}
			}
		}
		
		for (Monomial m1 : polinom1) {				// adunarea coef ramasi din polinom1
			if (m1.getPower() >= 0) {
				sum.add(m1);
			}
		}									
		for (Monomial m2 : polinom2) {
			if (m2.getPower() >= 0) {			// adunarea coef ramasi din polinom2
				sum.add(m2);
			}
		}
		String text = "";   					// obtinerea polinomului rez sub forma de text
		Collections.sort(sum, Monomial.Comparators.GRAD);
		for (Monomial m : sum) {
			if (m.getCoef() != 0.0) {
				if (m.getCoef() > 0) {
					text = text + "+";
					text = text + Double.toString(m.getCoef()) + "x^" + Integer.toString(m.getPower());
				} else {
					text = text + Double.toString(m.getCoef()) + "x^" + Integer.toString(m.getPower());
				}
			}
		}
		int db = 0;								//verificare coef polinom
		for (Monomial m : sum) {
			if (m.getCoef() != 0)
				db++;
			if (db == 0) {
				text = "0";
			}
		}
		
		out.setText(text);
		return sum;
	}

	ArrayList<Monomial> Subtract(String f1, String f2, JTextField out) {//scadere

		ArrayList<Monomial> result = new ArrayList<>();
		polinom1 = Split_Regex(f1);
		polinom2 = Split_Regex(f2);
		Collections.sort(polinom1, Monomial.Comparators.GRAD);
		Collections.sort(polinom2, Monomial.Comparators.GRAD);
		Monomial s = new Monomial(0.0, 0);
		for (Monomial m1 : polinom1) {
			for (Monomial m2 : polinom2) {
				if (m1.getPower() == m2.getPower()) {
					s.setCoef(m1.getCoef() - m2.getCoef());
					s.setPower(m1.getPower());
					m1.setPower(-1);
					m2.setPower(-1);
					result.add(s);
				}
			}
		}
		for (Monomial m1 : polinom1) {
			if (m1.getPower() >= 0.0) {
				result.add(m1);
			}
		}
		for (Monomial m2 : polinom2) {
			if (m2.getPower() >= 0.0) {
				double x = m2.getCoef();
				x = -x;
				m2.setCoef(x);
				result.add(m2);
			}
		}
		String text = "";
		Collections.sort(result, Monomial.Comparators.GRAD);
		for (Monomial m : result) {
			if (m.getCoef() != 0.0) {
				if (m.getCoef() > 0) {
					text = text + "+";
					text = text + Double.toString(m.getCoef()) + "x^" + Integer.toString(m.getPower());
				} else
					text = text + Double.toString(m.getCoef()) + "x^" + Integer.toString(m.getPower());
			}
		}
		int db = 0;
		for (Monomial m : result) {
			if (m.getCoef() != 0)
				db++;
			if (db == 0) {
				text = "0";
			}
		}
		out.setText(text);
		return result;
	}
	
	ArrayList<Monomial> Differentiate(String s, JTextField out) { //derivare
		polinom1 = Split_Regex(s);
		Collections.sort(polinom1, Monomial.Comparators.GRAD);
		for (Monomial m : polinom1) {
			if (m.getPower() == 0) {
				m.setCoef(0);
			}
			m.setCoef(m.getCoef() * m.getPower());
			m.setPower(m.getPower() - 1);
		}
		String text = "";
		Collections.sort(polinom1, Monomial.Comparators.GRAD);
		for (Monomial m : polinom1) {
			if (m.getCoef() != 0.0) {
				if (m.getCoef() > 0) {
					text = text + "+";
					text = text + Double.toString(m.getCoef()) + "x^" + Integer.toString(m.getPower());
				} else
					text = text + Double.toString(m.getCoef()) + "x^" + Integer.toString(m.getPower());
			}
			out.setText(text);
		}
		return polinom1;
	}

	ArrayList<Monomial> Integrate(String s, JTextField out) { //integrare
		polinom1 = Split_Regex(s);
		for (Monomial m : polinom1) {
			m.setPower(m.getPower() + 1);
			m.setCoef(m.getCoef() / m.getPower());
		}
		String text = "";
		Collections.sort(polinom1, Monomial.Comparators.GRAD);
		for (Monomial m : polinom1) {
			if (m.getCoef() != 0.0) {
				if (m.getCoef() > 0) {
					text = text + "+";
					text = text + Double.toString(m.getCoef()) + "x^" + Integer.toString(m.getPower());
				} else
					text = text + Double.toString(m.getCoef()) + "x^" + Integer.toString(m.getPower());
			}
			out.setText(text);
		}
		return polinom1;

	}

	ArrayList<Monomial> Multiply(String p1, String p2, JTextField out) {
		
		ArrayList <Monomial> multip = new ArrayList <>();
		polinom1 = Split_Regex(p1);
		polinom2 = Split_Regex(p2);
		Collections.sort(polinom1, Monomial.Comparators.GRAD);
		Collections.sort(polinom2, Monomial.Comparators.GRAD );
		int exp1=0, exp2=0;
		
		
		for (Monomial m1 : polinom1) {
			for (Monomial m2 : polinom2) {
				Monomial s=new Monomial();
				s.setCoef(m1.getCoef()*m2.getCoef());
				s.setPower(m1.getPower()+m2.getPower());
				multip.add(s);
			}
		}
		for (Monomial m1 : multip) {
		    exp2 = 0;
			for (Monomial m2 : multip) {
				if( (m1.getPower()==m2.getPower() ) && ( exp1 != exp2 ) ) {
					m1.setCoef( m1.getCoef()+m2.getCoef() );
					m2.setCoef(0);
				}
				exp2++;
			}
			exp1++;
		}
		String text="";
		Collections.sort(multip,  Monomial.Comparators.GRAD );
		for (Monomial m : multip) {
			if( m.getCoef()!=0.0 ) {
				if(m.getCoef()>0) {
					text = text + "+";
					text = text + Double.toString( m.getCoef() ) + "x^" + Integer.toString(m.getPower());
				}
				else
					text = text + Double.toString( m.getCoef() ) + "x^" + Integer.toString(m.getPower());
			}
			out.setText(text);
		}	
		return multip;
	}
	
	String JunitTest(String s) { //test junit derivare
		polinom1 = Split_Regex(s);
		Collections.sort(polinom1, Monomial.Comparators.GRAD);
		for (Monomial m : polinom1) {
			if (m.getPower() == 0) {
				m.setCoef(0);
			}
			m.setCoef(m.getCoef() * m.getPower());
			m.setPower(m.getPower() - 1);
		}
		String text = "";
		Collections.sort(polinom1, Monomial.Comparators.GRAD);
		for (Monomial m : polinom1) {
			if (m.getCoef() != 0.0) {
				if (m.getCoef() > 0) {
					text = text + "+";
					text = text + Double.toString(m.getCoef()) + "x^" + Integer.toString(m.getPower());
				} else
					text = text + Double.toString(m.getCoef()) + "x^" + Integer.toString(m.getPower());
			}
			
		}
		return text;
	}

}