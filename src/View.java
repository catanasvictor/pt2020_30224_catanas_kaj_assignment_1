import java.awt.BorderLayout;
import java.awt.GridLayout;
import javax.swing.*;

public class View {

	public View() {  //interfata GUI
		
		
		JFrame frame=new JFrame("Polynomial Calculator");
		frame.setSize(600, 200);
		
		frame.setLayout(new BorderLayout());
		
		JPanel panel1 = new JPanel();
		JPanel panel2 = new JPanel();
		
		JButton add_button = new JButton("+");
		JButton sub_button = new JButton("-");
		JButton multi_button = new JButton("*");
		JButton deriv_button = new JButton("()'");
		JButton integ_button = new JButton("Sdx");
		
		JTextField txt1 = new JTextField("");
		JTextField txt2 = new JTextField("");
		JTextField txt3 = new JTextField("");
		
		txt1.setSize(35, 130);
		txt2.setSize(35, 130);
		txt3.setSize(35, 200);
		
		Polynomial operation=new Polynomial();
		
		
		panel1.setLayout(new GridLayout(2, 4));
		JLabel label1 = new JLabel("Polinom 1:", JLabel.CENTER);
		panel1.add(label1);
		panel1.add(txt1);
		
		JLabel label3 = new JLabel("Result:", JLabel.CENTER);
		panel1.add(label3);
		panel1.add(txt3);
		
		JLabel label2 = new JLabel("Polinom 2:", JLabel.CENTER);
		panel1.add(label2);
		panel1.add(txt2);
		
		JLabel label4 = new JLabel("Input ex: ax^2-bx^1+cx^0", JLabel.CENTER);
		panel1.add(label4);
		
		
		panel2.setLayout(new GridLayout(1,6));
		
		add_button.addActionListener(e -> 
		{
			String s1=txt1.getText();
			String s2=txt2.getText();
			operation.Addition(s1,s2,txt3);
		}
		);
		
		sub_button.addActionListener(e -> 
		{
			String s1=txt1.getText();
			String s2=txt2.getText();
			operation.Subtract(s1,s2,txt3);
		}
		);
		
		multi_button.addActionListener(e -> 
		{
			String s1=txt1.getText();
			String s2=txt2.getText();
			operation.Multiply(s1,s2,txt3);
		}
		);
		
		deriv_button.addActionListener(e -> 
		{
			String s1=txt1.getText();
			operation.Differentiate(s1, txt3);
		}
		);
		
		integ_button.addActionListener(e -> 
		{
			String s1=txt1.getText();
			operation.Integrate(s1,txt3);
		}
		);
		
		panel2.add(add_button);
		panel2.add(sub_button);
		panel2.add(multi_button);
		panel2.add(deriv_button);
		panel2.add(integ_button);
		
		frame.add(panel1,BorderLayout.NORTH);
		frame.add(panel2,BorderLayout.CENTER);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
		
	}

}
