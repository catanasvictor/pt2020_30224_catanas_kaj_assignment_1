import java.util.Comparator;

public class Monomial implements Comparable<Monomial>{
	
		private double coef; // coeficient
		private int power;   // putere/exponent
		
		public Monomial() {
			this.coef = 0;
			this.power = 0;
		}
		public Monomial(double coef, int grad) {
			this.coef = coef;
			this.power = grad;
		}
		public int getPower() {
			return power;
		}
		public void setPower(int grad) {
			this.power = grad;
		}
		public double getCoef() {
			return coef;
		}
		public void setCoef(double coef) {
			this.coef = coef;
		}	
		
		public int compareTo(Monomial m) {
			return Comparators.GRAD.compare(this, m);
		}
		
	    public static class Comparators {
	    	
		        public static Comparator<Monomial> GRAD = new Comparator<Monomial>() {
		            @Override
		            public int compare(Monomial m1,Monomial m2) {
		               // return m2.getGrad().compareTo(m1.getGrad());
		                return m2.getPower() - m1.getPower();
		            }
		        };
		 }
	
}