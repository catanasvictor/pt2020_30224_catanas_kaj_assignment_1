import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class TestDiff {

	@Test
	void test() {
		
		Polynomial p=new Polynomial();
		String s="3x^5+2x^4";
		String differentiate=p.JunitTest(s);
		assertEquals("+15.0x^4+8.0x^3",differentiate);
		
		//testare derivare prin JunitTestCase
	}

}
